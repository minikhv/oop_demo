package demo.car;

public class VolgaCar extends Car{

    public Integer countWheel = 6;

    public VolgaCar() {
        super("Volga");
    }

    @Override
    public void go(Integer speed) {
        super.go(speed);
        System.out.println("Колес: " + countWheel);
    }
}
