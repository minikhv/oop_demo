package demo.car;

import demo.engine.Engine;

public abstract class Car {
     private String name;
     private Engine engine;

    public Car(String name) {
        this.name = name;
    }

    public void go(Integer speed) {
        if (engine == null) {
            System.out.println("Нет двигателя");
        } else {
            Integer finalSpeed = speed * engine.getSpeedScale();

            System.out.println("Скорость: " + finalSpeed + " км/ч");
            System.out.println("-----------");
            System.out.println("Марка авто: " + name);
            System.out.println("Двигатель: " + engine.getName());
        }
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
}
