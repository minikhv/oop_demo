package demo.car;

public class MoskvichCar extends Car{

    public Integer countWheel = 4;

    public MoskvichCar() {
        super("Moskvich");
    }

    @Override
    public void go(Integer speed) {
        super.go(speed);
        System.out.println("Колес: " + countWheel);
    }
}
