package demo;

import demo.car.*;
import demo.engine.*;

public class Main {
    public static void main(String[] args) {
        Car car = new MoskvichCar();
        Engine engine = new StupidEngine();

        car.setEngine(engine);

        car.go(100);

    }
}