package demo.engine;

public abstract class Engine {
    private String name;
    private Integer speedScale;

    public Engine(String name, Integer speedScale) {
        this.name = name;
        this.speedScale = speedScale;
    }

    public String getName() {
        return name;
    }

    public Integer getSpeedScale() {
        return speedScale;
    }
}
